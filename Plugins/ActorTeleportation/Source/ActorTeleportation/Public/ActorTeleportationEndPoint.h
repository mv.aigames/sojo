// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ActorTeleportationPackage.h"
#include "ActorTeleportationEndPoint.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, BlueprintType)
class UActorTeleportationEndPoint : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class ACTORTELEPORTATION_API IActorTeleportationEndPoint
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void OnReceive(const TScriptInterface<IActorTeleportationPackage>& pPackage);
	virtual void OnReceive_Implementation(const TScriptInterface<IActorTeleportationPackage>& pPackage) {}
};
