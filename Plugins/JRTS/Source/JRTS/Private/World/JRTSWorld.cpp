// Fill out your copyright notice in the Description page of Project Settings.


#include "World/JRTSWorld.h"

#include "Components/DirectionalLightcomponent.h"
#include "Components/SkyAtmosphereComponent.h"
#include "Components/SkyLightComponent.h"
#include "MathUtil.h"

#include "World/JRTSTime.h"
#include "World/JRTSDayNight.h"
#include "World/JRTSWeather.h"


AJRTSWorld::AJRTSWorld()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	defaultComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = defaultComponent;

	time = CreateDefaultSubobject<UJRTSTime>(TEXT("Time"));
	dayNight = CreateDefaultSubobject<UJRTSDayNight>(TEXT("DayNight"));
	dayNight->SetupAttachment(defaultComponent);
	wheather = CreateDefaultSubobject<UJRTSWeather>(TEXT("Wheather"));

	sun = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Sun"));
	sun->SetupAttachment(dayNight);

	moon = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Moon"));
	moon->SetupAttachment(dayNight);

	moon->ForwardShadingPriority = 1;
	FQuat r(FVector::RightVector, TMathUtilConstants<float>::Pi);
	moon->SetRelativeRotation(r);

	dayNight->sun = sun;
	dayNight->moon = moon;

	skylight = CreateDefaultSubobject<USkyLightComponent>(TEXT("SkyLight"));
	skylight->SetupAttachment(defaultComponent);

	skyAtmosphere = CreateDefaultSubobject<USkyAtmosphereComponent>(TEXT("SkyAtmosphere"));
	skyAtmosphere->SetupAttachment(defaultComponent);

}

// Called when the game starts or when spawned
void AJRTSWorld::BeginPlay()
{
	Super::BeginPlay();
	
	//UE_LOG(JRTSLog, Log, TEXT("World BeginPlay...."));
	FSimpleDelegate d = FTimeTickSignature::FDelegate::CreateUObject(this, &AJRTSWorld::OnTimeTick);
	time->timeTickEvents.Add(d);
}

FDelegateHandle AJRTSWorld::AddTimeTickDelegate(const TDelegate<void(AJRTSWorld*)>& pCallback)
{
	return worldTickEvent.Add(pCallback);
}

void AJRTSWorld::RemoveTimeTickDelegate(const FDelegateHandle& pDelegate)
{
	worldTickEvent.Remove(pDelegate);
}

// Called every frame
void AJRTSWorld::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AJRTSWorld::OnTimeTick()
{
	//UE_LOG(JRTSLog, Log, TEXT("World Tick...."));
	worldTickEvent.Broadcast(this);
}

float AJRTSWorld::GetTemperature()
{
	return 20.0f;
}

float AJRTSWorld::Gethour()
{
	return 0.0f;
}

bool AJRTSWorld::IsNight()
{
	return false;
}


