// Fill out your copyright notice in the Description page of Project Settings.


#include "World/JRTSDayNight.h"

#include "Components/DirectionalLightComponent.h"
#include "Kismet/GameplayStatics.h"

#include "World/JRTSWorld.h"

// Sets default values for this component's properties
UJRTSDayNight::UJRTSDayNight()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	
}


// Called when the game starts
void UJRTSDayNight::BeginPlay()
{
	Super::BeginPlay();

	// ...
	AJRTSWorld* w = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));
	if (w)
	{
		TDelegate<void(AJRTSWorld*)> d = FOnRTSWorldTickSignature::FDelegate::CreateUObject(this, &UJRTSDayNight::OnTimeTick);
		w->AddTimeTickDelegate(d);
	}
}


// Called every frame
void UJRTSDayNight::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UJRTSDayNight::OnTimeTick(AJRTSWorld* pWorld)
{
	UpdateRotation(pWorld);
}

void UJRTSDayNight::UpdateRotation(AJRTSWorld* pWorld)
{
	float minutes = (float)pWorld->GetMinutes();
	float minutesPerDay = (float)pWorld->GetMinutesPerDay();
	
	FQuat r(FVector::RightVector, FMath::DegreesToRadians((minutes / minutesPerDay) * 360.0f));
	SetRelativeRotation(r);
}
