// Fill out your copyright notice in the Description page of Project Settings.


#include "World/JRTSTime.h"

#include "Engine/World.h"

// Sets default values
UJRTSTime::UJRTSTime()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	
	PrimaryComponentTick.bCanEverTick = true;
}

void UJRTSTime::InitializeComponent()
{
	Super::InitializeComponent();


}

// Called when the game starts or when spawned
void UJRTSTime::BeginPlay()
{
	Super::BeginPlay();
	FTimerDelegate delegate = FTimerDelegate::CreateUObject(this, &UJRTSTime::OnTimeTick);
	GetWorld()->GetTimerManager().SetTimer(handle, delegate, 1.0f, true);
}

void UJRTSTime::OnTimeTick()
{
	//UE_LOG(JRTSLog, Log, TEXT("Time Tick...."));
	++totalTime;
	timeTickEvents.Broadcast();
}

// Called every frame
void UJRTSTime::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UJRTSTime::Pause()
{
	GetWorld()->GetTimerManager().PauseTimer(handle);
}

void UJRTSTime::Resume()
{
	GetWorld()->GetTimerManager().UnPauseTimer(handle);
}

