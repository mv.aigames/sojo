// Fill out your copyright notice in the Description page of Project Settings.


#include "Building/JRTSBuilding.h"

// Sets default values
AJRTSBuilding::AJRTSBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AJRTSBuilding::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AJRTSBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

