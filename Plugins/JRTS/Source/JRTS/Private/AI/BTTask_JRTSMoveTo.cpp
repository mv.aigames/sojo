// Fill out your copyright notice in the Description page of Project Settings.


#include "AI/BTTask_JRTSMoveTo.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "AIController.h"

#include "Controllers/JRTSAIController.h"
#include "LivingOrganims/JRTSHuman.h"

UBTTask_JRTSMoveTo::UBTTask_JRTSMoveTo()
{
	NodeName = "RTSMoveTo";
}

EBTNodeResult::Type UBTTask_JRTSMoveTo::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{

	EBTNodeResult::Type status = EBTNodeResult::Type::InProgress;

	AJRTSHuman* human = Cast<AJRTSHuman>(OwnerComp.GetAIOwner()->GetPawn());
	if (!human)
	{
		UE_LOG(LogTemp, Log, TEXT("RTS Warning: It must be a RTS Human"));
		return EBTNodeResult::Type::Failed;
	}

	AJRTSAIController* controller = Cast<AJRTSAIController>(OwnerComp.GetAIOwner());
	if (!controller)
	{
		UE_LOG(LogTemp, Log, TEXT("RTS Warning: It must be a RTS Controller"));
		return EBTNodeResult::Type::Failed;
	}

	status = DoMoveTask(OwnerComp, NodeMemory);
	

	return status;
}

EBTNodeResult::Type UBTTask_JRTSMoveTo::DoMoveTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	
	EPathFollowingRequestResult::Type result = EPathFollowingRequestResult::Type::Failed;
	if (BlackboardKey.SelectedKeyType == UBlackboardKeyType_Object::StaticClass())
	{
		AActor* actor = Cast<AActor>(OwnerComp.GetBlackboardComponent()->GetValueAsObject(GetSelectedBlackboardKey()));
		if (!actor)
		{
			UE_LOG(LogTemp, Log, TEXT("RTS Warning: Actor MoveTo null"));
			return EBTNodeResult::Type::Failed;
		}
		//EPathFollowingRequestResult::Type AAIController::MoveToActor(AActor * Goal, float AcceptanceRadius, bool bStopOnOverlap, bool bUsePathfinding, bool bCanStrafe, TSubclassOf<UNavigationQueryFilter> FilterClass, bool bAllowPartialPaths)
		result =  OwnerComp.GetAIOwner()->MoveToActor(actor, 1.0f, true, false, false);
	}

	if (BlackboardKey.SelectedKeyType == UBlackboardKeyType_Vector::StaticClass())
	{
		FVector target = OwnerComp.GetBlackboardComponent()->GetValueAsVector(GetSelectedBlackboardKey());
		result = OwnerComp.GetAIOwner()->MoveToLocation(target, 1.0f, true, false, false);
	}

	UE_LOG(LogTemp, Warning, TEXT("Result type: %d"), result);

	if (result == EPathFollowingRequestResult::Type::RequestSuccessful)
	{
		return EBTNodeResult::Type::InProgress;
	}
	else if (result == EPathFollowingRequestResult::Type::AlreadyAtGoal)
	{
		return EBTNodeResult::Succeeded;
	}
	else
	{
		return EBTNodeResult::Failed;
	}
}

void UBTTask_JRTSMoveTo::InitializeMemory(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTMemoryInit::Type InitType) const
{
	UE_LOG(LogTemp, Warning, TEXT("Initializing memory...."));
}

void UBTTask_JRTSMoveTo::CleanupMemory(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTMemoryClear::Type CleanupType) const
{
	UE_LOG(LogTemp, Warning, TEXT("Cleaning memory...."));
}

void UBTTask_JRTSMoveTo::OnGameplayTaskInitialized(UGameplayTask& Task)
{
	UE_LOG(LogTemp, Warning, TEXT("OnGameplayTaskInitialized memory...."));
}
