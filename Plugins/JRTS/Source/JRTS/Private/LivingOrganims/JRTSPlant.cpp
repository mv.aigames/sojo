// Fill out your copyright notice in the Description page of Project Settings.


#include "LivingOrganims/JRTSPlant.h"

#include "Kismet/GameplayStatics.h"

#include "World/JRTSWorld.h"
#include "LivingOrganims/JRTSOrganimsComponent.h"



AJRTSPlant::AJRTSPlant()
{
	PrimaryActorTick.bCanEverTick = false;

	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = sceneComponent;
}

// Called when the game starts or when spawned
void AJRTSPlant::BeginPlay()
{
	Super::BeginPlay();
	world = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));

	if (!world)
	{
		UE_LOG(JRTSLog, Error, TEXT("There is no RTS world"));
		return;
	}

	TDelegate<void(AJRTSWorld*)> d = FOnRTSWorldTickSignature::FDelegate::CreateUObject(this, &AJRTSPlant::OnWorldTick);
	worldHandle = world->AddTimeTickDelegate(d);

	if (!defaultOrganims)
	{
		UE_LOG(JRTSLog, Error, TEXT("A plant needs a organims"));
		return;
	}

	organims =  NewObject<UJRTSOrganimsComponent>(this, defaultOrganims);
	if (!organims)
	{
		UE_LOG(JRTSLog, Error, TEXT("Organims creation failed"));
	}
	organims->RegisterComponent();
}

void AJRTSPlant::BeginDestroy()
{
	Super::BeginDestroy();

	if (world)
	{
		world->RemoveTimeTickDelegate(worldHandle);
		world = nullptr;
	}
	
}

// Called every frame
void AJRTSPlant::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AJRTSPlant::OnWorldTick(class AJRTSWorld* pWorld)
{
	// Do here all the calculation here to consume plant's water and nutrients.
	// It might also be infected by a parasite.
}