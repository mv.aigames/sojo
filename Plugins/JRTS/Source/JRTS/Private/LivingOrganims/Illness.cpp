// Fill out your copyright notice in the Description page of Project Settings.


#include "LivingOrganims/Illness.h"

#include "Kismet/GameplayStatics.h"

#include "LivingOrganims/JRTSOrganimsComponent.h"
#include "World/JRTSWorld.h"

// Sets default values for this component's properties
UIllness::UIllness()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UIllness::BeginPlay()
{
	Super::BeginPlay();

	
	AActor* owner = GetOwner();
	if (owner)
	{
		organimsComponent = owner->FindComponentByClass<UJRTSOrganimsComponent>();
	}

	world = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));

	if (!world)
	{
		UE_LOG(JRTSLog, Error, TEXT("there is no RTS world"));
		return;
	}

	TDelegate<void(AJRTSWorld*)> d = FOnRTSWorldTickSignature::FDelegate::CreateUObject(this, &UIllness::OnWorldTick);
}


// Called every frame
void UIllness::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UIllness::OnWorldTick(AJRTSWorld* pWorld)
{
	if (organimsComponent)
	{

	}
}
