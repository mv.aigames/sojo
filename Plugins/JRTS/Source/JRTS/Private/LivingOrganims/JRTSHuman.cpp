// Fill out your copyright notice in the Description page of Project Settings.


#include "LivingOrganims/JRTSHuman.h"

#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"

#include "World/JRTSWorld.h"
#include "LivingOrganims/JRTSHumanMovementComponent.h"

// Sets default values
AJRTSHuman::AJRTSHuman()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	capsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	SetRootComponent(capsuleComponent);

	humanMovementComponent = CreateDefaultSubobject<UJRTSHumanMovementComponent>(TEXT("Human Move Component"));
	
	
	SetActorEnableCollision(false);
}

void AJRTSHuman::BeginDestroy()
{
	Super::BeginDestroy();
	rtsWorld->RemoveTimeTickDelegate(worldHandle);
}

// Called when the game starts or when spawned
void AJRTSHuman::BeginPlay()
{
	Super::BeginPlay();
	rtsWorld = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));
	if (!rtsWorld)
	{
		UE_LOG(JRTSLog, Log, TEXT("Cannot find RTS World"));
		return;
	}

	TDelegate<void(AJRTSWorld*)> d = FOnRTSWorldTickSignature::FDelegate::CreateUObject(this, &AJRTSHuman::OnTimeTick);
	worldHandle = rtsWorld->AddTimeTickDelegate(d);
}

void AJRTSHuman::OnTimeTick_Implementation(class AJRTSWorld* pWorld)
{
	//UE_LOG(JRTSLog, Log, TEXT("AJRTSHuman Tick...."));
}

// Called every frame
void AJRTSHuman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

FSelectorProperties AJRTSHuman::GetProperties()
{
	FSelectorProperties sp;
	sp.selector = this;
	return sp;
}
