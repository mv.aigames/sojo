// Fill out your copyright notice in the Description page of Project Settings.


#include "LivingOrganims/JRTSHumanMovementComponent.h"

// Sets default values for this component's properties
UJRTSHumanMovementComponent::UJRTSHumanMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UJRTSHumanMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UJRTSHumanMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	Velocity = GetPendingInputVector() + maxSpeed;
	UE_LOG(JRTSLog, Log, TEXT("Velocity: %s"), *Velocity.ToString());

	FVector VDelta = Velocity * DeltaTime;
	FHitResult hit(1.0f);
	//SafeMoveUpdatedComponent(VDelta, FQuat(), false, hit);
	MoveUpdatedComponent(VDelta, FQuat(), false, &hit);

	UpdateComponentVelocity();
	ConsumeInputVector();
}

void UJRTSHumanMovementComponent::AddInputVector(FVector WorldAccel, bool bForce /*=false*/)
{
	UE_LOG(JRTSLog, Log, TEXT("AddInputGVector...."));
	Super::AddInputVector(WorldAccel, bForce);
}

