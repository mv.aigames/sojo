// Fill out your copyright notice in the Description page of Project Settings.


#include "LivingOrganims/JRTSOrganimsComponent.h"

#include "Kismet/GameplayStatics.h"

#include "World/JRTSWorld.h"


// Sets default values for this component's properties
UJRTSOrganimsComponent::UJRTSOrganimsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UJRTSOrganimsComponent::BeginPlay()
{
	Super::BeginPlay();

	world = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));
	if (!world)
	{
		UE_LOG(JRTSLog, Error, TEXT("RTS world cannot be found"));
		return;
	}

	TDelegate<void(AJRTSWorld*)> d = FOnRTSWorldTickSignature::FDelegate::CreateUObject(this, &UJRTSOrganimsComponent::OnWorldTick);
	worldHandle = world->AddTimeTickDelegate(d);
}


// Called every frame
void UJRTSOrganimsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UJRTSOrganimsComponent::OnWorldTick(AJRTSWorld* pWorld)
{
	FOrganimsProperties op;
	op.water = 2;
	op.nutrients = 2;
	ConsumeProperties(op);
	//UE_LOG(JRTSLog, Log, TEXT("Curretn water: %d"), properties.water);
	if (IsDeath())
	{
		OnDeath();
	}
}

void UJRTSOrganimsComponent::OnDeath()
{
	world->RemoveTimeTickDelegate(worldHandle);
	world = nullptr;

	UE_LOG(JRTSLog, Log, TEXT("Organism from '%s' is death"), *GetOwner()->GetName());
}

bool UJRTSOrganimsComponent::ConsumeProperties(const FOrganimsProperties& pProperties)
{
	properties -= pProperties;

	if (IsDeath())
	{
		return false;
	}
	return true;
}