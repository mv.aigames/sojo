// Fill out your copyright notice in the Description page of Project Settings.


#include "JRTSPlayerController.h"

#include "Engine/World.h"
#include "Landscape.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/HUD.h"
#include "Kismet/GameplayStatics.h"

#include "Interface/JRTSInteractive.h"
#include "Assets/JRTSAssetPlacingGhost.h"

void AJRTSPlayerController::BeginPlay()
{
	landscape = Cast<ALandscape>(UGameplayStatics::GetActorOfClass(GetWorld(), ALandscape::StaticClass()));
	if (!landscape)
	{
		UE_LOG(JRTSLog, Warning, TEXT("A landscape is needed in order to work"));
	}

	if (!defaultAssetPlacingGhost)
	{
		UE_LOG(JRTSLog, Warning, TEXT("A ghost must be assigned"));
		return;
	}

	assetPlacingGhost = GetWorld()->SpawnActor<AJRTSAssetPlacingGhost>(defaultAssetPlacingGhost);

	if (!assetPlacingGhost)
	{
		UE_LOG(JRTSLog, Warning, TEXT("Fail to create ghost"));
	}

	assetPlacingGhost->SetActorHiddenInGame(false);
}

void AJRTSPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("QueryWorld", EInputEvent::IE_Pressed, this, &AJRTSPlayerController::OnQueryWorld);
}

void AJRTSPlayerController::OnQueryWorld()
{
	switch (mode)
	{
	case Mode::Normal:
		QueryWorld();
		break;
	case Mode::SpawningAsset:
		SpawningAsset();
		break;
	}
}

void AJRTSPlayerController::QueryWorld()
{
	if (isOnUI) { return; }

	FVector direction;
	FVector origin;
	DeprojectMousePositionToWorld(origin, direction);

	FHitResult hit;
	FCollisionQueryParams params;
	params.AddIgnoredActor(GetPawn());
	GetWorld()->LineTraceSingleByChannel(hit, origin, origin + direction * 1000.0f, ECollisionChannel::ECC_Visibility, params);

	IJRTSInteractive* selector = Cast<IJRTSInteractive>(hit.GetActor());

	if (hit.GetActor())
	{
		UE_LOG(LogTemp, Warning, TEXT("Actor: %s"), *hit.GetActor()->GetName());
	}
	selectorDelegate.Broadcast(selector);

	DrawDebugLine(GetWorld(), origin, origin + direction * 1000.0f, FColor::Cyan, true, 1, 0, 1.0f);
}

void AJRTSPlayerController::SpawningAsset()
{
	UE_LOG(JRTSLog, Log, TEXT("Placing asset id: %d"), selectedAssetId);
	selectedAssetId = -1;
	mode = Mode::Normal;
}

void AJRTSPlayerController::OnAssetSelected(int32 pAssetId)
{
	selectedAssetId = pAssetId;
	UE_LOG(JRTSLog, Log, TEXT("Selecting asset: %d"), selectedAssetId);
	mode = Mode::SpawningAsset;
	if (assetPlacingGhost)
	{
		assetPlacingGhost->SetActorHiddenInGame(false);
	}
}

void AJRTSPlayerController::OnGUI(bool pValue)
{
	isOnUI = pValue;
}

void AJRTSPlayerController::PostProcessInput(const float DeltaTime, const bool bGamePaused)
{
	Super::PostProcessInput(DeltaTime, bGamePaused);

	if (bGamePaused) { return; }

	if (mode == Mode::SpawningAsset)
	{
		UpdateAssetPosition();
	}
}

void AJRTSPlayerController::UpdateAssetPosition()
{
	if (landscape && assetPlacingGhost)
	{
		FVector direction;
		FVector origin;
		DeprojectMousePositionToWorld(origin, direction);
		const double cosValue = FVector::DownVector.Dot(direction);
		const double hipotenuse = origin.Z / cosValue;

		assetPlacingGhost->SetActorLocation(origin + direction * hipotenuse);
	}
}