// Fill out your copyright notice in the Description page of Project Settings.


#include "JRTSGameMode.h"

#include "JRTSPlayerController.h"
#include "UObject/ScriptInterface.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"

#include "World/JRTSWorld.h"

#include "UI/JRTSHUD.h"



void AJRTSGameMode::BeginPlay()
{
	//UE_LOG(LogTemp, Warning, TEXT("BeginPLay GameMode......."));
	AJRTSPlayerController* controller = Cast<AJRTSPlayerController>(GetWorld()->GetFirstPlayerController());
	if (controller)
	{
		
		//dSignature.BindRaw(this, &AJRTSGameMode::ReceiveJRTSInteractive);
		dSignature.BindLambda([&](IJRTSInteractive* pInteractive) {
			interactiveSelected.SetInterface(pInteractive);
			interactiveSelected.SetObject(Cast<UObject>(pInteractive));
			OnInteractiveSelected(interactiveSelected);
		});
		
		controller->selectorDelegate.Add(dSignature);
	}

	// Try to find RTSWorld
	rtsWorld = Cast<AJRTSWorld>(UGameplayStatics::GetActorOfClass(GetWorld(), AJRTSWorld::StaticClass()));
	if (rtsWorld)
	{
		UE_LOG(JRTSLog, Log, TEXT("RTS from level has been found..."));
	}
	else if (!rtsWorld && defaultRtsWorld)
	{
		UE_LOG(JRTSLog, Log, TEXT("Spawning RTSWorld..."));
		rtsWorld = GetWorld()->SpawnActor<AJRTSWorld>(defaultRtsWorld);
	}
}

void AJRTSGameMode::OnInteractiveSelected_Implementation(TScriptInterface<IJRTSInteractive> pSelector)
{
}

void AJRTSGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateHUD(); // TEMP
}

void AJRTSGameMode::UpdateHUD()
{
	UE_LOG(LogTemp, Warning, TEXT("Updating UI"));
	AJRTSHUD* hud = Cast<AJRTSHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());

	if (!hud) { return; }

	hud->DrawInteractive(interactiveSelected);
}