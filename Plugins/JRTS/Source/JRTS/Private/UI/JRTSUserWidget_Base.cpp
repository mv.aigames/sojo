// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/JRTSUserWidget_Base.h"

#include "Engine/World.h"

#include "JRTSPlayercontroller.h"

void UJRTSUserWidget_Base::NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseEnter(InGeometry, InMouseEvent);

	if (controller)
	{
		//UE_LOG(JRTSLog, Log, TEXT("UJRTSUserWidget_Base::NativeOnMouseEnter"));
		controller->OnGUI(true);
	}
}

void UJRTSUserWidget_Base::NativeOnMouseLeave(const FPointerEvent& InMouseEvent)
{
	Super::NativeOnMouseLeave(InMouseEvent);
	if (controller)
	{
		//UE_LOG(JRTSLog, Log, TEXT("UJRTSUserWidget_Base::NativeOnMouseLeave"));
		controller->OnGUI(false);
	}
}

void UJRTSUserWidget_Base::NativeConstruct()
{
	Super::NativeConstruct();

	controller = Cast<AJRTSPlayerController>(GetWorld()->GetFirstPlayerController());
	if (!controller)
	{
		UE_LOG(JRTSLog, Log, TEXT("This widget depends on JRTSPlayerController"));
		
	}
}

FReply UJRTSUserWidget_Base::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	
	return Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
}