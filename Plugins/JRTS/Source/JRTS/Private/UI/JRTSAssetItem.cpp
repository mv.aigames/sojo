// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/JRTSAssetItem.h"

#include "Components/CanvasPanel.h"
#include "Components/Image.h"
#include "Blueprint/WidgetTree.h"

#include "JRTSPlayerController.h"

FReply UJRTSAssetItem::NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	UWidget* w = GetRootWidget();
	if (w)
	{
		UE_LOG(JRTSLog, Log, TEXT("%s"), *w->GetName());
	}
	FReply reply = Super::NativeOnMouseButtonDown(InGeometry, InMouseEvent);
	if (InMouseEvent.IsMouseButtonDown(EKeys::LeftMouseButton))
	{
		return reply.Handled();
	}
	return reply;
}

void UJRTSAssetItem::NativePreConstruct()
{
	Super::NativePreConstruct();
	//panelRoot = Cast<UCanvasPanel>(GetRootWidget());
	if (WidgetTree)
	{
		PanelRoot = WidgetTree->ConstructWidget<UCanvasPanel>(UCanvasPanel::StaticClass(), TEXT("Canvas"));
		AssetIcon = WidgetTree->ConstructWidget<UImage>(UImage::StaticClass(), TEXT("Icon"));
		PanelRoot->AddChild(AssetIcon);
		PanelRoot->AddChildToCanvas(AssetIcon);
		WidgetTree->RootWidget = PanelRoot;
		
	}
	//WidgetTree->AddToRoot();
}



void UJRTSAssetItem::SetAssetId(int32 pAssetId)
{
	assetId = pAssetId;
}

FReply UJRTSAssetItem::NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent)
{
	UE_LOG(JRTSLog, Log, TEXT("NativeOnMouseButtonUp"));
	if (controller)
	{
		controller->OnAssetSelected(assetId);
	}
	return Super::NativeOnMouseButtonUp(InGeometry, InMouseEvent);
}