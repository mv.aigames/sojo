// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/JRTSHUD.h"

void AJRTSHUD::DrawInteractive(TScriptInterface<IJRTSInteractive> pInteractive)
{
	if (!pInteractive) { return; }
	OnDrawDisplayName(pInteractive);
	OnDrawActions(pInteractive);
}

void AJRTSHUD::OnDrawDisplayName_Implementation(TScriptInterface<IJRTSInteractive> pInteractive)
{
}

void AJRTSHUD::OnDrawActions_Implementation(TScriptInterface<IJRTSInteractive> pInteractive)
{
}