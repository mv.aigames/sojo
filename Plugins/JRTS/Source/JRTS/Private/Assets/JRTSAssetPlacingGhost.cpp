// Fill out your copyright notice in the Description page of Project Settings.


#include "Assets/JRTSAssetPlacingGhost.h"

#include "Components/StaticMeshComponent.h"

// Sets default values
AJRTSAssetPlacingGhost::AJRTSAssetPlacingGhost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	sceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = sceneComponent;

	meshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	meshComponent->SetupAttachment(sceneComponent);
}

// Called when the game starts or when spawned
void AJRTSAssetPlacingGhost::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AJRTSAssetPlacingGhost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

