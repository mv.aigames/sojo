// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTask_JRTSMoveTo.generated.h"

/**
 * 
 */
UCLASS()
class JRTS_API UBTTask_JRTSMoveTo : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, meta = (MinClamp = 0))
		float acceptableDistance = 1.0f;
	
public:
	UBTTask_JRTSMoveTo();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	virtual void InitializeMemory(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTMemoryInit::Type InitType) const;
	virtual void CleanupMemory(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTMemoryClear::Type CleanupType) const;
	virtual void OnGameplayTaskInitialized(UGameplayTask& Task) override;
	EBTNodeResult::Type DoMoveTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);
};
