// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interface/JRTSInteractive.h"
#include "JRTSBuilding.generated.h"

UCLASS()
class JRTS_API AJRTSBuilding : public AActor, public IJRTSInteractive
{
	GENERATED_BODY()

		UPROPERTY(BlueprintreadWrite, EditAnywhere, Category = Actions, meta = (AllowPrivateAccess = "true"))
		TArray<FString> actions;

	UPROPERTY(BlueprintreadWrite, EditAnywhere, Category = Actions, meta = (AllowPrivateAccess = "true"))
		FText displayName;
	
public:	
	// Sets default values for this actor's properties
	AJRTSBuilding();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UFUNCTION(BlueprintCallable)
		virtual TArray<FString> GetActions() override { return actions; }

	UFUNCTION(BlueprintCallable)
		virtual FText GetDisplayName() override { return displayName; }
};
