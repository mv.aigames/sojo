// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JRTSAssetPlacingGhost.generated.h"

UCLASS()
class JRTS_API AJRTSAssetPlacingGhost : public AActor
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		USceneComponent* sceneComponent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* meshComponent;
public:	
	// Sets default values for this actor's properties
	AJRTSAssetPlacingGhost();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
