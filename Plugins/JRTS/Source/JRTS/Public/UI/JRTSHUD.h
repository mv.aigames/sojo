// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"

#include "Interface/JRTSInteractive.h"

#include "JRTSHUD.generated.h"

/**
 * 
 */
UCLASS()
class JRTS_API AJRTSHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	void DrawInteractive(TScriptInterface<IJRTSInteractive> pInteractive);

public:
	UFUNCTION(BlueprintNativeEvent)
	void OnDrawDisplayName(const TScriptInterface<IJRTSInteractive>& pInteractive);
	virtual void OnDrawDisplayName_Implementation(TScriptInterface<IJRTSInteractive> pInteractive);

	UFUNCTION(BlueprintNativeEvent)
	void OnDrawActions(const TScriptInterface<IJRTSInteractive>& pInteractive);
	virtual void OnDrawActions_Implementation(TScriptInterface<IJRTSInteractive> pInteractive);
};
