// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "UI/JRTSUserWidget_Base.h"

#include "JRTSAssetItem.generated.h"

/**
 * 
 */
UCLASS()
class JRTS_API UJRTSAssetItem : public UJRTSUserWidget_Base
{
	GENERATED_BODY()
		UPROPERTY(VisibleAnywhere)
		int32 assetId = -1;

private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), meta = (BindWidget))
		class UCanvasPanel* PanelRoot;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (AllowPrivateAccess = "true"), meta = (BindWidget))
		class UImage* AssetIcon;

public:
	void SetAssetId(int32 pAssetId);


protected:
	virtual void NativePreConstruct() override;
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual FReply NativeOnMouseButtonUp(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	
};
