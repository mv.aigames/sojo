// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "JRTSUserWidget_Base.generated.h"

/**
 * 
 */
UCLASS()
class JRTS_API UJRTSUserWidget_Base : public UUserWidget
{
	GENERATED_BODY()

protected:
	class AJRTSPlayerController* controller;
	

protected:
	virtual void NativeConstruct() override;
	virtual void NativeOnMouseEnter(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
	virtual void NativeOnMouseLeave(const FPointerEvent& InMouseEvent) override;
	virtual FReply NativeOnMouseButtonDown(const FGeometry& InGeometry, const FPointerEvent& InMouseEvent) override;
};
