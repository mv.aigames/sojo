// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "Engine/DataTable.h"

#include "JRTSWeather.generated.h"

USTRUCT()
struct FWeatherAverage : public FTableRowBase
{
	GENERATED_BODY()
public:
	UPROPERTY(EDitAnywhere, BlueprintReadWrite, meta = (MinClamp = 0, MaxClamp = 1))
		float rainPorcentage = 0.0f;
	UPROPERTY(EDitAnywhere, BlueprintReadWrite, meta = (MinClamp = 0, MaxClamp = 1))
		float sunPorcentage = 0.0f;
	UPROPERTY(EDitAnywhere, BlueprintReadWrite)
		float  maxTemperature = 0.0f;
	UPROPERTY(EDitAnywhere, BlueprintReadWrite)
		float  minTemperature = 0.0f;
};


UCLASS( ClassGroup=(RTSWorld), BlueprintType, meta=(BlueprintSpawnableComponent) )
class JRTS_API UJRTSWeather : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UJRTSWeather();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
