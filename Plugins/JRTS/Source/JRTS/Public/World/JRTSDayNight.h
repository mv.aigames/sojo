// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"

#include "JRTSDayNight.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JRTS_API UJRTSDayNight : public USceneComponent
{
	GENERATED_BODY()

		friend class AJRTSWorld;

private:
	UPROPERTY(VisibleAnywhere)
		class UDirectionalLightComponent* sun;

	UPROPERTY(VisibleAnywhere)
		class UDirectionalLightComponent* moon;
	
public:	
	
	// Sets default values for this component's properties
	UJRTSDayNight();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnTimeTick(class AJRTSWorld* pWorld);
	void UpdateRotation(class AJRTSWorld* pWorld);
};
