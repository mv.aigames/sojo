// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "JRTSWorld.generated.h"

class UJRTSTime;
class UJRTSDayNight;
class UJRTSWeather;
class AJRTSWorld;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnRTSWorldTickSignature, AJRTSWorld*);

UCLASS()
class JRTS_API AJRTSWorld : public AActor
{
	GENERATED_BODY()
private:
	
private:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "World", meta = (AllowPrivateAccess = "true"))
	UJRTSTime* time;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "World", meta = (AllowPrivateAccess = "true"))
	UJRTSDayNight* dayNight;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "World", meta = (AllowPrivateAccess = "true"))
	UJRTSWeather* wheather;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "World", meta = (AllowPrivateAccess = "true"))
		class USkyLightComponent* skylight;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "World", meta = (AllowPrivateAccess = "true"))
		class USkyAtmosphereComponent* skyAtmosphere;

	UPROPERTY(VisibleAnywhere)
		class UDirectionalLightComponent* sun;

	UPROPERTY(VisibleAnywhere)
		class UDirectionalLightComponent* moon;

	USceneComponent* defaultComponent;

	
	
public:	
	// Sets default values for this actor's properties
	AJRTSWorld();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	FDelegateHandle AddTimeTickDelegate(const TDelegate<void(AJRTSWorld*)>& pCallback);
	void RemoveTimeTickDelegate(const FDelegateHandle& pDelegate);

	FOnRTSWorldTickSignature worldTickEvent;

public:
	float GetTemperature();
	float Gethour();
	bool IsNight();
	int32 GetMinutes() const { return time->GetMinutes(); }
	int32 GetMinutesPerDay() const { return 1440; }
private:
	//UFUNCTION()
	void OnTimeTick();

};
