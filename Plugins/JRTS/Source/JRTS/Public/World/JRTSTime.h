// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JRTSTime.generated.h"

//struct WheaterProperty
//{
//	FVector windVelocity;
//};

//DECLARE_MULTICAST_DELEGATE_OneParam(FUpdatetimeSiganture, const WheaterProperty&);
DECLARE_MULTICAST_DELEGATE(FTimeTickSignature);

UCLASS(ClassGroup = (RTSWorld), BlueprintType, meta = (BlueprintSpawnableComponent))
class JRTS_API UJRTSTime : public UActorComponent
{
	GENERATED_BODY()
	
private:
	UPROPERTY(VisibleAnywhere, Category = "Time")
		int32 totalTime = 0;

	FTimerHandle handle;

protected:
	virtual void BeginPlay() override;

public:	

	FTimeTickSignature timeTickEvents;

	void Pause();
	void Resume();

	int32 GetMinutes() const { return totalTime % 1440; }
private:
	void OnTimeTick();

public:
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void InitializeComponent();
	UJRTSTime();
};
