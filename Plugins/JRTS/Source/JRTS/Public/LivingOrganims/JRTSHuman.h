// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "Interface/JRTSInteractive.h"

#include "JRTSHuman.generated.h"




UCLASS()
class JRTS_API AJRTSHuman : public APawn, public IJRTSInteractive
{
	GENERATED_BODY()

		UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		class UCapsuleComponent* capsuleComponent;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		class UJRTSHumanMovementComponent* humanMovementComponent;

	UPROPERTY(BlueprintreadWrite, EditAnywhere, Category = Actions, meta = (AllowPrivateAccess = "true"))
		TArray<FString> actions;

	UPROPERTY(BlueprintreadWrite, EditAnywhere, Category = Actions, meta = (AllowPrivateAccess = "true"))
		FText displayName;

	UPROPERTY(VisibleAnywhere)
		class AJRTSWorld* rtsWorld;

	FDelegateHandle worldHandle;
	
public:	
	// Sets default values for this actor's properties
	AJRTSHuman();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

// IInteractive
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	virtual FSelectorProperties GetProperties() override;

	UFUNCTION(BlueprintCallable)
		virtual TArray<FString> GetActions() override { return actions; }

	UFUNCTION(BlueprintCallable)
		virtual FText GetDisplayName () override { return displayName; }
// IInteractive End

public:
	UFUNCTION(BlueprintNativeEvent)
		void OnTimeTick(class AJRTSWorld* pWorld);
	virtual void OnTimeTick_Implementation(class AJRTSWorld* pWorld);
};
