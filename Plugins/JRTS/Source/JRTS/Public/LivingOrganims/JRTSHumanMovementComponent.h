// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "JRTSHumanMovementComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JRTS_API UJRTSHumanMovementComponent : public UPawnMovementComponent
{
	GENERATED_BODY()

		UPROPERTY(BlueprintREadWrite, EditAnywhere, Category = HumanMovement, meta = (AllowPrivateAccess = "true", ClampMin = "0", UIMin = "0", ForceUnits = "cm/s"))
		float maxSpeed = 100.0f;

public:	
	// Sets default values for this component's properties
	UJRTSHumanMovementComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void AddInputVector(FVector WorldAccel, bool bForce = false) override;

		
};
