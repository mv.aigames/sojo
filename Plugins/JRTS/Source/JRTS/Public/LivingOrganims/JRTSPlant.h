// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Interface/JRTSInteractive.h"

#include "JRTSPlant.generated.h"

UCLASS(ClassGroup=(LivingOrganims))
class JRTS_API AJRTSPlant : public AActor
{
	GENERATED_BODY()

	

	UPROPERTY(VisibleAnywhere, Category = World)
	class AJRTSWorld* world;

	FDelegateHandle worldHandle;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = Components, meta = (AllowprivateAccess = "true"))
		USceneComponent* sceneComponent;

	UPROPERTY(EditAnywhere, Category = Biology)
		TSubclassOf<class UJRTSOrganimsComponent> defaultOrganims;


	UPROPERTY(VisibleAnywhere, Category = Biology)
		class UJRTSOrganimsComponent* organims;

public:	
	// Sets default values for this actor's properties
	AJRTSPlant();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void OnWorldTick(class AJRTSWorld* pWorld);

};
