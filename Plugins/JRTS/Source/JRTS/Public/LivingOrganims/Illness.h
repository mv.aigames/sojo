// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Illness.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class JRTS_API UIllness : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UIllness();

	class AJRTSWorld* world;
	FDelegateHandle worldHandle;

	class UJRTSOrganimsComponent* organimsComponent;
private:
	void OnWorldTick(class AJRTSWorld* pWorld);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
