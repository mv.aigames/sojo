// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JRTSOrganimsComponent.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FOrganimsProperties
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere)
		int32 water = 100;

	UPROPERTY(VisibleAnywhere)
		int32 nutrients = 100;
	
};

FOrganimsProperties operator-(const FOrganimsProperties& pLeft, const FOrganimsProperties& pRight)
{
	FOrganimsProperties rv;
	rv.water = pLeft.water - pRight.water;
	rv.nutrients = pLeft.nutrients - pRight.nutrients;
	return rv;
}

FOrganimsProperties& operator-=(FOrganimsProperties& pValue, const FOrganimsProperties& pRight)
{
	pValue.water -= pRight.water;
	pValue.nutrients -= pRight.nutrients;
	return pValue;
}


UCLASS( ClassGroup=(LivingOrganims), meta=(BlueprintSpawnableComponent), BlueprintType)
class JRTS_API UJRTSOrganimsComponent : public UActorComponent
{
	GENERATED_BODY()


	/** Max value of healthy water that the plant can tolerate. Units in mililiter. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Biology, meta = (AllowPrivateAccess = "true"))
		int32 maxWater = 100;

	UPROPERTY(VisibleAnywhere, Category = Biology)
		FOrganimsProperties properties;

	UPROPERTY(VisibleAnywhere, Category = World)
	class AJRTSWorld* world;

	FDelegateHandle worldHandle;
public:	
	// Sets default values for this component's properties
	UJRTSOrganimsComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void OnWorldTick(class AJRTSWorld* pWorld);
	void OnDeath();

	bool ConsumeProperties(const FOrganimsProperties& pProperties);

	bool IsDeath() const noexcept { return properties.water <= 0 || properties.nutrients <= 0; }
};
