// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"



#include "JRTSGameMode.generated.h"

class IJRTSInteractive;

DECLARE_DELEGATE_OneParam(FInteractiveDelegateSignature, IJRTSInteractive*);

/**
 * 
 */
UCLASS()
class JRTS_API AJRTSGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		TScriptInterface<IJRTSInteractive> interactiveSelected;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, meta = (AllowPrivateAccess = "true"))
		TSubclassOf<class AJRTSWorld> defaultRtsWorld;

	class AJRTSWorld* rtsWorld;
	
protected:
	virtual void BeginPlay() override;
	FInteractiveDelegateSignature dSignature;

public:
	UFUNCTION(BlueprintNativeEvent)
		void OnInteractiveSelected(const TScriptInterface<IJRTSInteractive>& pInteractive);
	virtual void OnInteractiveSelected_Implementation(TScriptInterface<IJRTSInteractive> pInteractive);

	virtual void Tick(float DeltaTime) override;

private:
	void UpdateHUD();
};
