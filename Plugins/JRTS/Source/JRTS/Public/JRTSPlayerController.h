// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "JRTSPlayerController.generated.h"

class IJRTSInteractive;

DECLARE_MULTICAST_DELEGATE_OneParam(FSelectorSiganture, IJRTSInteractive*);
/**
 * 
 */
UCLASS()
class JRTS_API AJRTSPlayerController : public APlayerController
{
	GENERATED_BODY()
	enum class Mode
	{
		Normal,
		SpawningAsset,
	};

	Mode mode = Mode::Normal;

	int32 selectedAssetId = -1;
	bool isOnUI = false;

	UPROPERTY(VisibleAnywhere)
		class ALandscape* landscape;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AJRTSAssetPlacingGhost> defaultAssetPlacingGhost;

	UPROPERTY(VisibleAnywhere)
		class AJRTSAssetPlacingGhost* assetPlacingGhost;

protected:
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;
	virtual void PostProcessInput(const float DeltaTime, const bool bGamePaused) override;

private:
	void OnQueryWorld();
	void QueryWorld();
	void SpawningAsset();
	void UpdateAssetPosition();
	

public:
	FSelectorSiganture selectorDelegate;
	void OnAssetSelected(int32 pAssetId);
	void OnGUI(bool pValue);
};
