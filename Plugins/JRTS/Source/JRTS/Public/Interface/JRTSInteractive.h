// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "JRTSInteractive.generated.h"


USTRUCT(BlueprintType)
struct FSelectorProperties
{
	GENERATED_BODY();
	class IJRTSInteractive* selector;
};

// This class does not need to be modified.
UINTERFACE(MinimalAPI, NotBlueprintable, BlueprintType)
class UJRTSInteractive : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class JRTS_API IJRTSInteractive
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable)
	virtual FSelectorProperties GetProperties() { return FSelectorProperties(); }

	UFUNCTION(BlueprintCallable)
	virtual void PrintHello() {}

	UFUNCTION(BlueprintCallable)
	virtual TArray<FString> GetActions() { return TArray<FString>(); };

	UFUNCTION(BlueprintCallable)
		virtual FText GetDisplayName() { return FText(); }
		
};
