// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SOJOGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SOJO_API ASOJOGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
