// Copyright Epic Games, Inc. All Rights Reserved.

#include "SOJO.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SOJO, "SOJO" );
